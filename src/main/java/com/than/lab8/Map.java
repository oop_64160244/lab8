package com.than.lab8;

public class Map {
    private String name;
    private int width ;
    private int height ;
    public Map(String name,int width,int height){
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public void printMap() {
        for(int y = 1;y<=height;y++){
            for(int x = 1;x<=width;x++){
                System.out.print("-");
            }
            System.out.println();
        }
    }

    public String getName(){
        return name;
    }
    
    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }
}
