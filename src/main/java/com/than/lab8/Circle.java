package com.than.lab8;

public class Circle {
    private double radius;
    private String name;

    public Circle(String name,double radius){
        this.name = name ;
        this.radius = radius;
    }
    
    public double calCircleArea(){
        double area = 0;
        area = Math.PI*(radius*radius);
        return area;
    }

    public void printCircleArea(){
        System.out.println(name +" area" + " = " + calCircleArea());
    }

    public String getName(){
        return name;
    }

    public double getRadius(){
        return radius;
    }

    public double calCircleperimeter(){
        double perimeter = 0;
        perimeter = Math.PI*radius*2;
        return perimeter; 
    }

    public void printCirclePerimeter(){
        System.out.println(name+" perimeter = " + calCircleperimeter());
    }
}

