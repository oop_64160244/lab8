package com.than.lab8;

public class Square {
    private double width;
    private double height;
    private String name;
    
    public Square(String name,double width,double height){
        this.name = name ;
        this.width = width;
        this.height = height;
    }

    public double getWidth(){
        return width;
    }

    public double getHeight(){
        return height;
    }

    public double calSquareArea(){
        double area =  0;
        area = width * height;
        return area;
    }
    public void printSquareArea(){
        System.out.println(name+ " area" + " = " + calSquareArea());
    }

    public String getName(){
        return name;
    }

    public double calSquarePerimeter(){
        double perimeter = 0;
        perimeter = width*2 + height*2;
        return perimeter;
    }
    
    public void printSquarePerimeter(){
        System.out.println(name + " perimeter = " + calSquarePerimeter());
    }
}
