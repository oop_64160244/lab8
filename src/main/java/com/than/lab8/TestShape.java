package com.than.lab8;

public class TestShape {
    public static void main(String[] args) {
        Square square1 = new Square("square1",10,5);
        System.out.println(square1.getName()+" Width"+" = "+square1.getWidth());
        System.out.println(square1.getName()+" Height"+" = "+square1.getHeight());
        square1.calSquareArea();
        square1.printSquareArea();
        square1.calSquarePerimeter();
        square1.printSquarePerimeter();

        Square square2 = new Square("square2",5,3);
        System.out.println(square2.getName()+" Width"+" = "+square2.getWidth());
        System.out.println(square2.getName()+" Height"+" = "+square2.getHeight());
        square2.calSquareArea();
        square2.printSquareArea();
        square2.calSquarePerimeter();
        square2.printSquarePerimeter();
        
        Circle circle1 = new Circle("circle1",1);
        System.out.println(circle1.getName()+" radius = "+circle1.getRadius());
        circle1.calCircleArea();
        circle1.printCircleArea();
        circle1.calCircleperimeter();
        circle1.printCirclePerimeter();

        Circle circle2 = new Circle("circle2",2);
        System.out.println(circle2.getName()+" radius = "+circle2.getRadius());
        circle2.calCircleArea();
        circle2.printCircleArea();
        circle2.calCircleperimeter();
        circle2.printCirclePerimeter();

        Triangle triangle1 = new Triangle("triangle1", 5, 5, 6);
        System.out.println(triangle1.getName()+" a = "+triangle1.getA() + " b = "+triangle1.getB() + " c = " + triangle1.getC());
        triangle1.calTriArea();
        triangle1.printTriArea();
        triangle1.calTriPerimeter();
        triangle1.printTriPerimeter();
    }   
}
